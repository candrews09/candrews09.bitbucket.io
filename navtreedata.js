/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Cole Andrews ME405 Winter 2021", "index.html", [
    [ "PROJECTS", "index.html#sec_intro", null ],
    [ "Lab0x01 Vending machine.", "index.html#sec_lab0x01", null ],
    [ "Lab0x02  Reaction Challenge.", "index.html#sec_lab0x02", null ],
    [ "Lab0x03 ADC Button", "index.html#sec_lab0x03", null ],
    [ "Lab0x04 Hot or Not?", "index.html#sec_lab0x04", null ],
    [ "Lab0x05 Balancing Platform Kinematics", "index.html#sec_lab0x05", null ],
    [ "Lab0x06 Simulation or Reality", "index.html#sec_lab0x06", null ],
    [ "Lab0x07 Touch", "index.html#sec_lab0x07", null ],
    [ "Lab0x08 Term Project Part 1", "index.html#sec_lab0x08", null ],
    [ "Lab0x09 Term Project Part 2", "index.html#sec_lab0x09", null ],
    [ "FBDs and Kinematics for the Balancing Platform", "page1.html", null ],
    [ "System Simulation Results", "page2.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Encoder_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';