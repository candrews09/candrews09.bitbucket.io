var MotorDriver_8py =
[
    [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ],
    [ "CH1", "MotorDriver_8py.html#a6d3f7625cf6be01a0858344d600f1faf", null ],
    [ "CH2", "MotorDriver_8py.html#ae1add7b33fa8c74ec702db7864a2f51a", null ],
    [ "CH3", "MotorDriver_8py.html#aa8a2ecdfcdd1d132adacbe2b9a97d5b9", null ],
    [ "CH4", "MotorDriver_8py.html#a7eb108cd9964693f5fd5621aa71de722", null ],
    [ "IN1", "MotorDriver_8py.html#a8c9da946115ac1fd5f771e242016fce6", null ],
    [ "IN2", "MotorDriver_8py.html#a8a0d921223036fd65fdaea12473c08ec", null ],
    [ "IN3", "MotorDriver_8py.html#a0b30d4934d889e0c9b18a300e735dde7", null ],
    [ "IN4", "MotorDriver_8py.html#a7ff8d15dc46254c444cd6f70d72c9495", null ],
    [ "Mot1_y", "MotorDriver_8py.html#acc3d759ca542a31f2b429d6666a68a86", null ],
    [ "Mot2_x", "MotorDriver_8py.html#a05b3b5618f64949c7ece116062ead09e", null ],
    [ "nFAULT_pin", "MotorDriver_8py.html#aa27d0ecde94ffa4567175af474f8081e", null ],
    [ "nSLEEP", "MotorDriver_8py.html#a844049d370e2494076878e17c5bad6f7", null ],
    [ "timer_3", "MotorDriver_8py.html#a5d3b76c5aaf0a9f3505dd5d5fd45cd1c", null ]
];